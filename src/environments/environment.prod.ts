export const environment = {
  production: true,
  apibaseurl:'https://my-json-server.typicode.com/karthik1918/EON-App',
  Violation:[
    {
      "type": "line",
      "title": "Risk index",
      "subtitle": "Machine 14",
      "count": "51",
      "countof": "/100",
      "period": "2.4%",
      "perioddesc": "Less than last month",
      "svgiconstyle": "transform:scaleY(1)",
      "iconimg": "assets/icons/arrow_downward.svg",
      "roundclass": "roundyellow",
      "svgcolor": "green",
      "video":"assets/videos/1.png"
    },
    {
      "type": "line",
      "title": "Safety line voilation",
      "subtitle": "Machine 15",
      "count": "58",
      "countof": "Total alerts per month",
      "secondcount": "12",
      "secondcountof": "Alerts",
      "period": "4%",
      "perioddesc": "Less than last month",
      "svgiconstyle": "transform:scaleY(1)",
      "iconimg": "assets/icons/arrow_downward.svg",
      "roundclass": "roundgreen",
      "svgcolor": "green",
      "video":"assets/videos/2.png"
    },
    {
      "type": "line",
      "title": "Crowd violations",
      "subtitle": "Machine 15",
      "count": "28",
      "countof": "/100",
      "period": "3%",
      "perioddesc": "More than last month",
      "svgiconstyle": "transform:scaleY(-1)",
      "iconimg": "assets/icons/arrow_downward.svg",
      "roundclass": "roundred",
      "svgcolor": "red",
      "video":"assets/videos/3.png"
    },
    {
      "type": "employee",
      "title": "Employees scanned",
      "subtitle": "Machine 13",
      "count": "236",
      "countof": "Total alerts per month",
      "secondcount": "16",
      "secondcountof": "Alerts",
      "period": "7%",
      "perioddesc": "More than yesterday",
      "svgiconstyle": "transform:scaleY(-1)",
      "iconimg": "assets/icons/arrow_downward.svg",
      "roundclass": "roundred",
      "svgcolor": "red",
      "video":"assets/videos/4.png"
    }
  ],
  Alerts:[
    {'title':'Safety line crossing','time':'15:28:06','place':'Workstation 14','count':'3 people'},
    {'title':'Crowd violations','time':'15:18:06','place':'Near admin','count':'2 people'},
    {'title':'Safety line crossing','time':'14:28:06','place':'Workstation 5','count':'1 person'},
    {'title':'Safety line crossing','time':'14:05:06','place':'Workstation 14','count':'3 people'},
    {'title':'Crowd violations','time':'13:28:06','place':'Entrance','count':'2 people'},
    {'title':'Safety line crossing','time':'13:15:06','place':'Workstation 6','count':'2 people'},
    {'title':'Safety line crossing','time':'12:28:06','place':'Workstation 9','count':'2 people'}
  ],
  Report:{'addr1':'06','addr2':'05','addr3':'06'}
};
