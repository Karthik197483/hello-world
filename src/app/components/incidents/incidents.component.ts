import { Component, OnInit } from '@angular/core';
import { environment } from '@env/environment';
import { MyjsonapiService } from './../../services/myjsonapi.service';
declare var jQuery: any;
@Component({
  selector: 'app-incidents',
  templateUrl: './incidents.component.html',
  styleUrls: ['./incidents.component.css']
})
export class IncidentsComponent implements OnInit {
  linedata = environment.Violation;
  alerts =[];// environment.Alerts;
  report:any;// =environment.Report;
  constructor(
      private myjsonapiService: MyjsonapiService
  ) {
      this.loadviolation();
      this.loadalerts();
      this.loadreport();
      setTimeout(() => {
          this.bindevents();
      }, 3000);
  }
  ngOnInit(): void {
      
  }
  bindevents()
  {
      (function ($) {
          $(document).ready(function () {
            $('.card-link1').click(function () {
                // $(this).find('i').toggleClass('fa fa-angle-down fa fa-angle-up');
                var icon = $(this).find('.material-icons');
                 icon.toggleClass('up');
                if ( icon.hasClass('up') ) {
                    icon.text('expand_less');
                  } else {
                    icon.text('expand_more');
                  }
            });
              var coll = document.getElementsByClassName("collapsible");
              var i;

              for (i = 0; i < coll.length; i++) {
                  coll[i].addEventListener("click", function () {
                      this.classList.toggle("active");
                      var content = this.nextElementSibling;
                      if (content.style.maxHeight) {
                          content.style.maxHeight = null;
                      } else {
                          content.style.maxHeight = content.scrollHeight + "px";
                      }
                  });
              }
              var coll = document.getElementsByClassName("collapsible1");
              var i;

              for (i = 0; i < coll.length; i++) {
                  coll[i].addEventListener("click", function () {
                      this.classList.toggle("active");
                      var content = this.nextElementSibling;
                      if (content.style.display === "block") {
                          content.style.display = "none";
                      } else {
                          content.style.display = "block";
                      }
                  });
              }
          });
      })(jQuery);
  }
  loadviolation() {
      this.myjsonapiService.getviolation().then(data => {
          var resultdata = JSON.parse(data['_body']);
          //console.log(resultdata);
          this.linedata = resultdata;
      }).catch(d => { })
          .finally(() => { });
  }
  loadalerts() {
      this.myjsonapiService.getalerts().then(data => {
          var resultdata = JSON.parse(data['_body']);
          //console.log(resultdata);
          this.alerts = resultdata;
      }).catch(d => { })
          .finally(() => { });
  }
  loadreport() {
      this.myjsonapiService.getreport().then(data => {
          var resultdata = JSON.parse(data['_body']);
          //console.log(resultdata);
          this.report = resultdata;
      }).catch(d => { })
          .finally(() => { });
  }
}
