import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { environment } from '@env/environment';
import { MyjsonapiService } from './../../services/myjsonapi.service';
declare var jQuery: any;
@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    linedata = [];//  environment.Violation;
    alerts = [];// environment.Alerts;
    report: any = { 'addr1': '', 'addr2': '', 'addr3': '' };// =environment.Report;
    lasralertclickId: '';
    constructor(
        private route: Router,
        private myjsonapiService: MyjsonapiService
    ) {
        this.loadviolation();
        this.loadalerts();
        this.loadreport();
        setTimeout(() => {
            this.bindevents();
        }, 3000);
    }
    ngOnInit(): void {

    }
    bindevents() {
        (function ($) {
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
                $('.card-link').click(function () {
                    // $(this).find('i').toggleClass('fa fa-angle-down fa fa-angle-up');
                    var icon = $(this).find('.material-icons');
                    icon.toggleClass('up');
                    if (icon.hasClass('up')) {
                        icon.text('expand_less');
                    } else {
                        icon.text('expand_more');
                    }
                });
                $('.card-link1').click(function () {
                    // $(this).find('i').toggleClass('fa fa-angle-down fa fa-angle-up');
                    var icon = $(this).find('.material-icons');
                    icon.toggleClass('up');
                    if (icon.hasClass('up')) {
                        icon.text('expand_less');
                    } else {
                        icon.text('expand_more');
                    }
                });
                var coll = document.getElementsByClassName("collapsible");
                var i;

                for (i = 0; i < coll.length; i++) {
                    coll[i].addEventListener("click", function () {
                        this.classList.toggle("active");
                        var content = this.nextElementSibling;
                        if (content.style.maxHeight) {
                            content.style.maxHeight = null;
                        } else {
                            content.style.maxHeight = content.scrollHeight + "px";
                        }
                    });
                }
                // var coll = document.getElementsByClassName("collapsible1");
                // var i;

                // for (i = 0; i < coll.length; i++) {
                //     coll[i].addEventListener("click", function () {
                //         this.classList.toggle("active");
                //         var content = this.nextElementSibling;
                //         if (content.style.display === "block") {
                //             content.style.display = "none";
                //         } else {
                //             content.style.display = "block";
                //         }
                //     });
                // }
            });
        })(jQuery);
    }
    loadviolation() {
        this.myjsonapiService.getviolation().then(data => {
            var resultdata = JSON.parse(data['_body']);
            //console.log(resultdata);
            this.linedata = resultdata;
        }).catch(d => { })
            .finally(() => { });
    }
    loadalerts() {
        this.myjsonapiService.getalerts().then(data => {
            var resultdata = JSON.parse(data['_body']);
            //console.log(resultdata);
            this.alerts = resultdata;
        }).catch(d => { })
            .finally(() => { });
    }
    loadreport() {
        this.myjsonapiService.getreport().then(data => {
            var resultdata = JSON.parse(data['_body']);
            //console.log(resultdata);
            this.report = resultdata;
        }).catch(d => { })
            .finally(() => { });
    }
    navigateTo(page) {
        let navigationExtras: NavigationExtras = {
        };
        this.route.navigate([page], navigationExtras);
    }
    openNewwindowPage(page) {
        const url = this.route.serializeUrl(
            this.route.createUrlTree([page])
        );

        window.open(url, '_blank');
    }
    notificationclick(event) {
        var target = event.target || event.srcElement || event.currentTarget;
        jQuery(target).toggleClass('active');
        //jQuery(target).attr('fill', '#0072BC');
        // jQuery(target).children().css('filter', 'grayscale(100%) sepia(100%);');
        // if (jQuery(target).attr('href') == "assets/icons/notifications.svg")
        //     jQuery(target).attr('href', "assets/icons/notifications-blue.svg");
        // else
        //     jQuery(target).attr('href', "assets/icons/notifications.svg");
    }
    alertclick(event, i) {
        //alert(jQuery('#divcontent0').css('display'))
        var display = jQuery('#divcontent' + i).css('display');
        jQuery('.content2').hide();
        if (display == 'none')
            jQuery('#divcontent' + i).show();
        else
            jQuery('#divcontent' + i).hide();
    }
    popalertactive(event) {
        // var target =  event.srcElement;
        // setTimeout(() => {
        //     jQuery(target).toggleClass('active');    
        // }, 200);
        
    }
    closepopalerts(id,target) {
        var source="#svg"+id;
        jQuery('.txtcontrol').val('');
        jQuery('.popalerts').removeClass("show");
        jQuery('.svgalert').removeClass('active');
        //jQuery(source >'.svgalert').toggleClass("active");
       
        setTimeout(() => {
            if (this.lasralertclickId != target) {
                jQuery(target).addClass("show");
                jQuery(source).addClass("active");
                this.lasralertclickId = target;
            }
            else
            {
                jQuery(target).removeClass("show");
                jQuery(source).removeClass("active");
                this.lasralertclickId='';
            }
        }, 200);

    }
}
