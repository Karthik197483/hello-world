import { TestBed } from '@angular/core/testing';

import { MyjsonapiService } from './myjsonapi.service';

describe('MyjsonapiService', () => {
  let service: MyjsonapiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MyjsonapiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
