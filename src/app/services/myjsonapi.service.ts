import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { environment } from '@env/environment';
@Injectable({
  providedIn: 'root'
})
export class MyjsonapiService {
  endpointurl = environment.apibaseurl;
  constructor(
    private http: Http
  ) {

  }

  async getviolation() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this.http.get(this.endpointurl+'/Violation', options).toPromise();
  }
  async getalerts() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this.http.get(this.endpointurl+'/alerts', options).toPromise();
  }
  async getreport() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Accept', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this.http.get(this.endpointurl+'/report', options).toPromise();
  }
}
